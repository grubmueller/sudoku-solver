/*
MIT License

Copyright (c) 2020 Fabian Lukas Grubmüller, Lukas Bartl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <array>
#include <iomanip>
#include <string>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <cassert>

struct Coo {std::size_t row, col;}; // coordinates

inline std::ostream& operator<<(std::ostream& os, Coo const coo) {
    return os << '(' << coo.row << ',' << coo.col << ')';
}


template <std::size_t N>
class Cell
{
  static constexpr std::size_t NN = N*N;
  
private:
  bool corrupt_ {};
  std::size_t value_ {}; // value_ = 1, …, NN; 0 means, value is not set
  std::size_t left_ {NN}; // number of fitting values
  std::array<bool, NN> fitting_ {}; // fitting_[i-1] is true iff value i fits

public:
  Cell()
  {
    fitting_.fill(true);
  }

  bool corrupt() const noexcept {return corrupt_;}
  std::size_t value() const noexcept {return value_;}  
  std::size_t left() const noexcept {return left_;}
  std::array<bool, NN> const& fitting() const noexcept {return fitting_;}

  // convert to bool, true if valid, false if corrupt
  explicit operator bool() const noexcept {return !corrupt_;}

  // assign new value, returns *this
  Cell& assign(std::size_t const val) // val = 1, …, NN
  {
    assert(val > 0 && val <= NN);

    if (fitting_[val-1])
      {
	value_ = val;
	fitting_.fill(false);
	left_ = 0;
      }
    else
      corrupt_ = true;

    return *this;
  }

  // update left_ and fitting_, returns *this
  Cell& notify(std::size_t const val) // val = 1, …, NN
  {
    assert(val > 0 && val <= NN);

    if (fitting_[val-1])
      {
	fitting_[val-1] = false;
	--left_;
	if (!left_ && !value_) // no fitting values left, but value is zero
	  corrupt_ = true;
      }
    
    return *this;
  }
};

template <std::size_t N>
class Sudoku
{
  static constexpr std::size_t NN = N*N;
  
private:
  bool corrupt_ {};
  std::array<std::array<Cell<N>, NN>, NN> grid_ {};

public:
  Sudoku() = default;

  bool corrupt() const noexcept {return corrupt_;}

  // convert to bool, true if valid, false if corrupt
  explicit operator bool() const noexcept {return !corrupt_;}

  // assign new value and notify Cells, returns *this
  Sudoku& assign(Coo const coo, std::size_t const val)
  {
    bool valid = !corrupt_ && grid_[coo.row][coo.col].assign(val);

    for (std::size_t col = 0; col<NN; ++col) // notify all cells in row
      valid = valid && grid_[coo.row][col].notify(val);

    for (std::size_t row = 0; row<NN; ++row) // notify all cells in column
      valid = valid && grid_[row][coo.col].notify(val);

    Coo const bound = {(coo.row / N) * N + N, (coo.col / N) * N + N}; // block upper bound

    for (std::size_t row = (coo.row / N) * N; row<bound.row; ++row)
      for (std::size_t col = (coo.col / N) * N; col<bound.col; ++col)
	valid = valid && grid_[row][col].notify(val);

    corrupt_ = !valid;

    return *this;
  }

  std::size_t value(Coo const coo) const noexcept {return grid_[coo.row][coo.col].value();}
  std::size_t left(Coo const coo) const noexcept {return grid_[coo.row][coo.col].left();}
  std::array<bool, NN> const& fitting(Coo const coo) const noexcept {return grid_[coo.row][coo.col].fitting();}

  friend std::ostream& operator<<(std::ostream& ostream, Sudoku const& s)
  {
    if (s.corrupt())
      ostream << "CORRUPT\n";
    
    for (auto const& row : s.grid_)
      {
	for (auto const& cell : row)
	  ostream << cell.value() << ' ';

	ostream << '\n';
      }

    return ostream;
  }

  friend std::istream& operator>>(std::istream& istream, Sudoku& s)
  {
    for (std::size_t row=0; row<NN; ++row)
      for (std::size_t col=0; col<NN; ++col)
	if (std::size_t temp; istream >> temp && temp)
	  s.assign(Coo{row, col}, temp);
    
    return istream;
  }
};

template <std::size_t N>
class Cell_ranking
{
  static constexpr std::size_t NN = N*N;
  
private:
  Sudoku<N>& s;
  std::vector<Coo> ranking_ {};

  constexpr auto compare_pred() const
  {
    return [this] (Coo const coo1, Coo const coo2) {
        return s.left(coo1) > s.left(coo2);
    };
  }
  
public:
  Cell_ranking(Sudoku<N>& s): s(s) {}

  Cell_ranking(Cell_ranking const&) = delete;
  Cell_ranking& operator=(Cell_ranking const&) = delete;

  Cell_ranking(Sudoku<N>& s, Cell_ranking const& r): s(s), ranking_(r.ranking_) {}
  Cell_ranking(Sudoku<N>& s, Cell_ranking&& r): s(s), ranking_(std::move(r.ranking_)) {}

  friend std::ostream& operator<<(std::ostream& os, Cell_ranking const& c)
  {
    for (Coo coo : c.ranking_)
      os << coo << ' ';

    return os;
  }
  
  Coo top() const
  {
    assert(!ranking_.empty());

    return ranking_.back();
  }

  void pop()
  {
    assert(!ranking_.empty());

    ranking_.pop_back();
  }

  void add (Coo const coo) // add new entry
  {
    ranking_.insert(std::lower_bound(ranking_.begin(), ranking_.end(), coo, compare_pred()), coo);
  }

  void update() // update all entrys = sort ranking_
  {
    std::stable_sort(ranking_.begin(), ranking_.end(), compare_pred());
  }

  std::size_t size() const noexcept {return ranking_.size();}
  bool empty() const noexcept {return ranking_.empty();}
};

// assign val and update cell_ranking, returns if s is valid
template <std::size_t N>
bool cell_assign (Sudoku<N>& s, Cell_ranking<N>& cell_ranking, Coo const coo, std::size_t const val)
{
  if (s.assign(coo, val))
    {
      cell_ranking.update();
      return true;
    }
  else
      return false;
}

/*
// zu müde
struct Value {std::size_t val, cnt;}; */

// assign a value if there is a cell with only one possible value
template <std::size_t N>
bool cell_unit_eliminate (Sudoku<N>& s, Cell_ranking<N>& cell_ranking)
{
  if (Coo const top = cell_ranking.top(); s.left(top) == 1)
    {
      cell_ranking.pop();
      
      auto const& fitting = s.fitting(top);
      
      std::size_t val;
      for (val = 0; !fitting[val]; ++val);

      cell_assign(s, cell_ranking, top, val+1);

      return true;
    }
  else
      return false;
}

template <std::size_t N>
bool group_unit_eliminate(Sudoku<N>& s, Cell_ranking<N>& cell_ranking)
{
  // TODO
  return false;
}

// solve Sudoku s with guessing, returns if s is solved
template <std::size_t N>
bool sudoku_solve_with_guessing(Sudoku<N>& s, Cell_ranking<N>& cell_ranking)
{
  Coo const top = cell_ranking.top();
  cell_ranking.pop();

  auto const& fitting = s.fitting(top);

  for (std::size_t val = 0, left = s.left(top); left; --left, ++val)
    {
      for (; !fitting[val]; ++val);
      
      Sudoku<N> s_new = s;
      Cell_ranking<N> cell_ranking_new {s_new, cell_ranking};
      
      cell_assign(s_new, cell_ranking_new, top, val+1);

      if (sudoku_solve_helper(s_new, cell_ranking_new))
	{
	  s = s_new;
	  return true;
	}
    }
  
  // no assignment was succesful
  return false;
}

// solve Sudoku s with cell_ranking, returns if s is solved
template <std::size_t N>
bool sudoku_solve_helper (Sudoku<N>& s, Cell_ranking<N>& cell_ranking)
{
  while (!cell_ranking.empty() && s && (cell_unit_eliminate(s, cell_ranking) || group_unit_eliminate(s, cell_ranking)));

  if (s.corrupt()) // s is corrupt
    return false;
  else if (!cell_ranking.empty() && !sudoku_solve_with_guessing(s, cell_ranking)) // we need to guess
    return false;
  else // s is solved
    return true;
}


// solve Sudoku s, returns if solved
template <std::size_t N>
bool sudoku_solve (Sudoku<N>& s)
{
  if (s.corrupt())
    {
      std::cerr << "Unable to solve sudoku: CORRUPT DATA\n";
      return false;
    }
  
  constexpr std::size_t NN = N*N;
  
  Cell_ranking<N> cell_ranking {s};

  for (std::size_t row = 0; row < NN; ++row)
    for (Coo coo {row, 0}; coo.col < NN; ++coo.col)
      if (s.left(coo))
        cell_ranking.add(coo);

  /*
  // Keine Konzentration mehr vorhanden
  std::vector<Value> value_ranking;
  for (std::size_t val=1; i<=NN; ++)
  value_ranking.push_back(Value {i, … */

  return sudoku_solve_helper(s, cell_ranking);
}


int main(int const argc, char const* argv[])
{
    if (argc < 2) {
      std::cerr << "No input file specified\n";
      return EXIT_FAILURE;
    }

    char const*const sudoku_name = argv[1];
    std::ifstream sudoku_is {sudoku_name};

    if (!sudoku_is) {
      std::cerr << "Unable to read file " << sudoku_name << '\n';
      return EXIT_FAILURE;
    }

    Sudoku<3> sudoku;

    if (!(sudoku_is >>sudoku)) {
        std::cerr << "Failed to read sudoku " << sudoku_name << '\n';
        return EXIT_FAILURE;
    } else if (sudoku.corrupt()) {
        std::cerr << "Sudoku " << sudoku_name << " corrupt\n";
        return EXIT_FAILURE;
    }

    std::cout << sudoku_name << ":\n" << sudoku << std::endl;

    if (sudoku_solve(sudoku)) {
        std::cout << "Mögliche Lösung:\n" << sudoku;
        return EXIT_SUCCESS;
    } else {
        std::cerr << "Sudoku nicht lösbar!\n";
        return EXIT_FAILURE;
    }
}
